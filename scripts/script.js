$(() => {
    $(document).on("click", ".page-header-navigation-item", function (event) {
        const elemToScrollTo = $(event.target.attributes["href"].nodeValue);
        $("html").animate({scrollTop: elemToScrollTo.offset().top}, "slow");
    });

    $(window).on("scroll", function (event) {
        if ($(document).scrollTop() > $(window).innerHeight()) {
            $(".scroll_icon").removeClass("hidden_item");
        } else {
            $(".scroll_icon").addClass("hidden_item");
        }
    });

    $(".scroll_icon").on("click", function () {
        $("html").animate({scrollTop: 0}, "slow");
    });

    $(".btn_hide").on("click", function () {
        $(".top-rated").toggleClass("hidden_item");
    })
});

